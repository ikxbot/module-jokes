<?php
namespace Ikx\Jokes\Command;

use Ikx\Core\Application;
use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class DadjokeCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = true;

    public function describe()
    {
        return "Dadjokes";
    }

    public function run()
    {
        $useragent = sprintf('IkxBot IRC Bot v%s (https://gitlab.com/ikxbot) - (%s PHP %s)',
            Application::VERSION, gethostname(), php_uname('v') . ' ' . php_uname('m'));

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL             => 'https://icanhazdadjoke.com/',
            // CURLOPT_USERAGENT       => $useragent,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HEADER          => false,
            CURLOPT_HTTPHEADER      => [
                'Accept: text/plain'
            ]
        ]);
        $buffer = curl_exec($ch);
        curl_close($ch);

        $this->msg($this->channel, sprintf("%s: %s",
            Format::bold('Dad joke'), trim($buffer)));
    }
}